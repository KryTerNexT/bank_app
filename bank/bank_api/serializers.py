from rest_framework import serializers
from . import models


class CurrencySerializer(serializers.ModelSerializer):
    """
    Currency serializer
    """

    class Meta:
        model = models.Currency
        fields = ('nominal', 'cash_count')


class WithdrawSerializer(serializers.Serializer):
    """
    Withdraw serializer
    """
    amount = serializers.IntegerField(min_value=0)
