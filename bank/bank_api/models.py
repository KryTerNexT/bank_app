from django.db import models


# Create your models here.

class Currency(models.Model):
    """
    Available currencies in a bank
    """

    class Meta:
        # Show normal plural name in Admin page
        verbose_name_plural = 'Currencies'

    nominal = models.PositiveIntegerField(unique=True)
    cash_count = models.PositiveIntegerField(default=0)

    def __repr__(self):
        return "Currency(nominal={}, count={})".format(self.nominal, self.cash_count)

    def __str__(self):
        return "{}: {}".format(self.nominal, self.cash_count)
