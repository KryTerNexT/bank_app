from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from . import serializers
from . import models


class BankSetApiView(viewsets.ViewSet):

    def create(self, request):
        # currencies dict
        currencies = {}

        for nominal, count in request.data.items():
            try:
                # check for integer
                nominal = int(nominal)
                count = int(count)
                if nominal <= 0 or count < 0:
                    return Response(
                        {"detail": "All values should be positive integers"},
                        status=status.HTTP_400_BAD_REQUEST
                    )

                # add currency to the dict
                currencies[nominal] = count
            except ValueError as e:
                return Response(
                    {"detail": str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )

        for nominal, count in currencies.items():
            # save new currencies into a database
            currency, _ = models.Currency.objects.get_or_create(nominal=nominal)
            currency.nominal = nominal
            currency.cash_count += count
            currency.save()

        # Redirect to the bank status
        return redirect('bank_api:bank_status')


class StatusApiView(viewsets.ViewSet):
    """
    Check status of bank
    """

    def list(self, request):
        # get all available currencies in DB
        queryset = models.Currency.objects.all().order_by("-nominal")

        currencies = {}

        for currency in queryset:
            currencies[currency.nominal] = currency.cash_count

        return Response(currencies, status=status.HTTP_200_OK)


class WithdrawApiViewSet(viewsets.ViewSet):
    """
    Withdraw View
    """

    serializer_class = serializers.WithdrawSerializer

    def create(self, request):

        # serialize our request
        serializer = serializers.WithdrawSerializer(data=request.data)

        if serializer.is_valid():
            balance = 0
            currencies = models.Currency.objects.all()
            currencies_dict = {}

            # Calculate available balance
            for c in currencies:
                nominal = c.nominal
                cash_count = c.cash_count
                currencies_dict[nominal] = cash_count
                balance += nominal * cash_count

            # get amount from request
            amount = serializer.data.get('amount')

            # Check requested amount with available balance
            if amount > balance:
                return Response(
                    {'detail': 'There is not enough money in the bank'},
                    status=status.HTTP_406_NOT_ACCEPTABLE
                )

            else:
                need_to_give = amount
                withdraw_dict = {}

                # Go through all available currencies in decrease order
                for nominal in sorted(currencies_dict.keys(), reverse=True):
                    available_cash_count = currencies_dict[nominal]
                    cash_count = 0
                    while need_to_give >= nominal and available_cash_count > 0:
                        cash_count += 1
                        available_cash_count -= 1
                        currencies_dict[nominal] -= 1
                        need_to_give -= nominal
                        withdraw_dict[nominal] = cash_count

                # If bank has requested amount
                if need_to_give == 0:
                    for nominal, count in currencies_dict.items():
                        # update currency in DB
                        currency, _ = models.Currency.objects.update_or_create(nominal=nominal)
                        currency.cash_count = count
                        currency.save()
                    return Response(withdraw_dict, status=status.HTTP_200_OK)
                else:
                    return Response(
                        {'detail': 'Sorry! Not enough cash in the bank.'},
                        status=status.HTTP_406_NOT_ACCEPTABLE
                    )
        else:
            return Response(
                {'detail': 'Amount should be positive integer value'},
                status=status.HTTP_400_BAD_REQUEST
            )
