from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import api_views

app_name = 'bank_api'

router = DefaultRouter()

# API urls

urlpatterns = [
    path('', include(router.urls)),
    path('bank/status/', api_views.StatusApiView.as_view({'get': 'list'}), name='bank_status'),
    path('bank/set/', api_views.BankSetApiView.as_view({'post': 'create'}), name='bank_set'),
    path('withdraw/', api_views.WithdrawApiViewSet.as_view({'post': 'create'}), name='withdraw')
]
