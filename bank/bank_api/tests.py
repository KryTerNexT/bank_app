from django.test import TestCase
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status


class BankTests(APITestCase):

    def test_bank_set(self):
        bank_set_url = reverse('bank_api:bank_set')
        status_url = reverse('bank_api:bank_status')

        data = {'200': '10', '20': '1'}
        bank_set_request = self.client.post(bank_set_url, data)
        get_status_request = self.client.get(status_url)

        self.assertEqual(get_status_request.data, {200: 10, 20: 1})

        data = {'2w': 20}
        bank_set_request = self.client.post(bank_set_url, data)
        self.assertEqual(bank_set_request.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'200': '12'}

        bank_set_request = self.client.post(bank_set_url, data)
        get_status_request = self.client.get(status_url)

        self.assertEqual(get_status_request.data, {200: 22, 20: 1})

    def test_withdraw(self):
        bank_set_url = reverse('bank_api:bank_set')
        bank_withdraw_url = reverse('bank_api:withdraw')

        data = {'200': '10', '20': '1'}
        bank_set_request = self.client.post(bank_set_url, data)

        data = {'amount': 200}
        get_withdraw = self.client.post(bank_withdraw_url, data)

        self.assertEqual(get_withdraw.data, {200: 1})

        data = {'amount': -10}
        get_withdraw = self.client.post(bank_withdraw_url, data)

        self.assertEqual(get_withdraw.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'amount': 1}
        get_withdraw = self.client.post(bank_withdraw_url, data)

        self.assertEqual(get_withdraw.status_code, status.HTTP_406_NOT_ACCEPTABLE)
