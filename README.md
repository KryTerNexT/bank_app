#Bank API task


###Project Structure

    bank/
        bank_api/
            migrations/
            admin.py
            api_views.py          # All an API views
            apps.py
            models.py
            serializers.py        # API serializers
            test.py               # Unit tests
            urls.py
            views.py
        
        bank/
            settings.py
            urls.py
            wsgi.py
        
        manage.py
    
    .gitignore
    requirements.txt
